import random
import sys
import unittest
from copy import deepcopy

import torch
from deap import tools

import torchx.evo.datasets as datasets
from torchx.evo.evolution.coevolution import CoEvolution
from torchx.evo.evolution.configuration import (
    DNNEvoConfiguration,
    DNNTrainConfiguration,
)
from torchx.evo.evolution.process import Evaluator, PopulationEvolution
from torchx.evo.evolution.toolbox import (
    ToolboxBuilder,
    TrainerAwareFitness,
    signature,
)
from torchx.evo.genotype import (
    Gene,
    Genotype,
    NUMBER_OF_FIELDS,
)
from torchx.evo.intermediate import genotype_to_layers, Layer
from torchx.evo.nn import (
    remove_unnecessary_connections,
    populate_dependencies,
    evaluate_model_accuracy,
    wrap_with_nn_module,
    translate_to_model,
    evaluate_as_nn,
    GraphNodeModule,
    GraphModule,
)
from torchx.evo.utils.promises import (
    MemoryValuePromise,
    TargetDiffValuePromise,
    AvgCompositeValuePromise,
)


def layer_type_num(element):
    target = Layer.types.index(element)
    step = 1.0 / len(Layer.types)
    layer_type = target * step
    return layer_type


def test_directory_prefix():
    return "/tmp/test"


datasets.datasets_directory_prefix = test_directory_prefix
torch.manual_seed(7)
random.seed(1)


def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


class EvolutionWithConvolutionTest(unittest.TestCase):

    parameters_mnist = {
        "dataset": "mnist",
        "dense_size_multiplier": 100,
        "convolution_size_multiplier": 5,
        "batch_size": 64,
        "train_iterations": 1,
        "learning_rate": 0.1,
        "momentum": 0.00,
        "activation_fn": 'relu',
        "rng_seed": "1"
    }

    parameters_cifar10 = {
        "dataset": "cifar10",
        "dense_size_multiplier": 100,
        "convolution_size_multiplier": 5,
        "batch_size": 64,
        "train_iterations": 1,
        "learning_rate": 0.1,
        "momentum": 0.00,
        "activation_fn": 'relu',
        "rng_seed": "1"
    }

    def _evaluate_without_exceptions(self, genotype, parameters, subset=None):
        if subset is None:
            subset = [10, 100, 1000, 10000]

        try:
            result = evaluate_as_nn(genotype, parameters, subset)
            return result
        except Exception:
            self.fail("Expecting no exceptions!")

    def test_evaluation_returns_tuple_with_float(self):
        genotype = [
            0.6084712646407436, 0.9994313202604522,
            0.12722497113745512, 0.1931516601051776,
            0.4547980831823486
        ]

        parameters = {
            'batch_size': '128',
            'convolution_size_multiplier': '1',
            'dataset': 'mnist',
            'dense_size_multiplier': '1',
            'learning_rate': '0.1',
            'momentum': '0.0',
            'optimizer': 'sgd',
            'rng_seed': '-1',
            'train_iterations': '1'
        }

        subset = [10, 100, 1000, 10000]

        result = self._evaluate_without_exceptions(
            genotype,
            parameters,
            subset
        )

        self.assertIs(type(result), tuple)
        self.assertIs(type(result[0]), float)

    def test_concatenate_layers_of_different_dims(self):

        genotype = [
            0.6084712646407436, 0.9994313202604522,
            0.12722497113745512, 0.1931516601051776,
            0.4547980831823486,
            0.635253612323626, 0.7949058472039329,
            0.6549117516061397, 0.7481074043381254,
            0.3843745090299925,
            0.03552737075412349, 0.46076149263996646,
            0.32497289787050343, 0.4675198423772925,
            0.4641563304117835,
            0.2953017049136283, 0.7927822318395594,
            0.8190103706666555, 0.48120108421522134,
            0.9322371548320924
        ]

        parameters = {
            'batch_size': '128',
            'convolution_size_multiplier': '1',
            'dataset': 'mnist',
            'dense_size_multiplier': '1',
            'learning_rate': '0.1',
            'momentum': '0.0',
            'optimizer': 'sgd',
            'rng_seed': '-1',
            'train_iterations': '1'
        }

        self._evaluate_without_exceptions(genotype, parameters)

    def test_dense_output_smaller_than_kernel(self):
        parameters = EvolutionWithConvolutionTest.parameters_mnist.copy()

        layer0 = Layer(0, layer_type_num(Layer.DENSE))
        layer0.add_extra(0.0)
        layer0.mark_entry_layer()

        layer1 = Layer(2, layer_type_num(Layer.CONVOLUTION))
        layer1.add_extra(0.5)
        layer1.mark_exit_layer()

        gnm0 = GraphNodeModule(layer0, parameters, [])
        gnm1 = GraphNodeModule(layer1, parameters, [gnm0])
        nn_module = GraphModule(gnm1, parameters)

        try:
            evaluate_model_accuracy(
                nn_module,
                [10, 100, 1000, 10000],
                parameters
            )
        except Exception:
            self.fail("Expecting no exceptions!")

    def test_convolution_1d_as_last_layer(self):
        parameters = EvolutionWithConvolutionTest.parameters_mnist.copy()

        # output: 100
        layer0 = Layer(0, layer_type_num(Layer.DENSE))
        layer0.add_extra(1.0)
        layer0.mark_entry_layer()

        layer1 = Layer(2, layer_type_num(Layer.CONVOLUTION))
        layer1.add_extra(0.5)
        layer1.mark_exit_layer()

        gnm0 = GraphNodeModule(layer0, parameters, [])
        gnm1 = GraphNodeModule(layer1, parameters, [gnm0])
        nn_module = GraphModule(gnm1, parameters)

        try:
            evaluate_model_accuracy(
                nn_module,
                [10, 100, 1000, 10000],
                parameters
            )
        except Exception:
            self.fail("Expecting no exceptions!")

    def test_convolution_2d_with_3_channels_as_input(self):
        parameters = EvolutionWithConvolutionTest.parameters_cifar10.copy()

        genotype = [
            # single output gene
            0.5, 0.5, 0.0, 1.0, layer_type_num(Layer.CONVOLUTION)
        ]

        self._evaluate_without_exceptions(genotype, parameters)

    def test_convolution_2d_as_input(self):
        parameters = EvolutionWithConvolutionTest.parameters_mnist.copy()

        genotype = [
            # single output gene
            0.5, 0.5, 0.0, 1.0, layer_type_num(Layer.CONVOLUTION)
        ]

        self._evaluate_without_exceptions(genotype, parameters)

    def test_extend_with_linear_when_convolution_2d_as_output(self):
        parameters = EvolutionWithConvolutionTest.parameters_mnist.copy()

        genotype = [
            # single output gene
            0.5, 0.5, 1.0, 1.0, layer_type_num(Layer.CONVOLUTION)
        ]

        self._evaluate_without_exceptions(genotype, parameters)


class GraphNodeModuleTest(unittest.TestCase):

    def test_two_dependencies_dense_conv_1d_input_conv1d(self):
        parameters = {
            'convolution_size_multiplier': 10,
            'dense_size_multiplier': 100,
            'dataset': 'cifar10'
        }

        # output: 100
        layer0 = Layer(0, layer_type_num(Layer.DENSE))
        layer0.add_extra(1.0)
        layer0.mark_entry_layer()

        # 10 channels x 30x30 after first convolution -> 9000 outputs
        layer1 = Layer(1, layer_type_num(Layer.CONVOLUTION))
        layer1.add_extra(1.0)
        layer1.mark_entry_layer()

        layer2 = Layer(2, layer_type_num(Layer.CONVOLUTION))
        layer2.add_extra(0.5)

        gnm0 = GraphNodeModule(layer0, parameters, [])
        gnm1 = GraphNodeModule(layer1, parameters, [])
        gnm2 = GraphNodeModule(layer2, parameters, [gnm0, gnm1])
        output_shape = gnm2.output_shape

        self.assertEqual((5, 9098), output_shape)

    def test_two_dependencies_1d_input_conv1d(self):
        parameters = {
            'convolution_size_multiplier': 10,
            'dense_size_multiplier': 100,
            'dataset': 'cifar10'
        }

        layer0 = Layer(0, layer_type_num(Layer.DENSE))
        layer0.add_extra(1.0)
        layer0.mark_entry_layer()

        layer1 = Layer(1, layer_type_num(Layer.DENSE))
        layer1.add_extra(1.0)
        layer1.mark_entry_layer()

        layer2 = Layer(1, layer_type_num(Layer.CONVOLUTION))
        layer2.add_extra(0.5)

        gnm0 = GraphNodeModule(layer0, parameters, [])
        gnm1 = GraphNodeModule(layer1, parameters, [])
        gnm2 = GraphNodeModule(layer2, parameters, [gnm0, gnm1])
        output_shape = gnm2.output_shape

        self.assertEqual((5, 198), output_shape)

    def test_one_dependency_1d_input_conv1d(self):
        parameters = {
            'convolution_size_multiplier': 10,
            'dense_size_multiplier': 100,
            'dataset': 'cifar10'
        }

        layer0 = Layer(0, layer_type_num(Layer.DENSE))
        layer0.add_extra(1.0)
        layer0.mark_entry_layer()

        layer1 = Layer(1, layer_type_num(Layer.CONVOLUTION))
        layer1.add_extra(0.5)

        gnm0 = GraphNodeModule(layer0, parameters, [])
        gnm1 = GraphNodeModule(layer1, parameters, [gnm0])
        output_shape = gnm1.output_shape

        self.assertEqual((5, 98), output_shape)

    def test_one_dependency_2d_input_dense(self):
        parameters = {
            'convolution_size_multiplier': 10,
            'dense_size_multiplier': 100,
            'dataset': 'cifar10'
        }

        layer0 = Layer(0, layer_type_num(Layer.CONVOLUTION))
        layer0.add_extra(1.0)
        layer0.mark_entry_layer()

        layer1 = Layer(1, layer_type_num(Layer.DENSE))
        layer1.add_extra(0.5)

        gnm0 = GraphNodeModule(layer0, parameters, [])
        gnm1 = GraphNodeModule(layer1, parameters, [gnm0])
        output_shape = gnm1.output_shape

        self.assertEqual((50,), output_shape)

    def test_one_dependency_2d_input_conv2d(self):
        parameters = {
            'convolution_size_multiplier': 10,
            'dense_size_multiplier': 100,
            'dataset': 'cifar10'
        }

        layer_type = layer_type_num(Layer.CONVOLUTION)
        layer0 = Layer(0, layer_type)
        layer0.add_extra(1.0)
        layer0.mark_entry_layer()

        layer1 = Layer(1, layer_type)
        layer1.add_extra(0.5)

        gnm0 = GraphNodeModule(layer0, parameters, [])
        gnm1 = GraphNodeModule(layer1, parameters, [gnm0])
        output_shape = gnm1.output_shape

        self.assertEqual((5, 28, 28), output_shape)

    def test_input_shape_2d_output_shape_conv2d_cifar10(self):
        parameters = {
            'convolution_size_multiplier': 10,
            'dense_size_multiplier': 100,
            'dataset': 'cifar10'
        }

        layer_type = layer_type_num(Layer.CONVOLUTION)
        layer = Layer(1, layer_type)
        layer.add_extra(1.0)
        layer.mark_entry_layer()

        gnm = GraphNodeModule(layer, parameters, [])
        output_shape = gnm.output_shape

        self.assertEqual((10, 30, 30), output_shape)

    def test_input_shape_2d_output_shape_conv2d(self):
        # mnist input shape (X, 1, 28, 28)
        parameters = {
            'convolution_size_multiplier': 10,
            'dense_size_multiplier': 100,
            'dataset': 'mnist'
        }

        layer_type = layer_type_num(Layer.CONVOLUTION)
        layer = Layer(1, layer_type)
        layer.add_extra(1.0)
        layer.mark_entry_layer()

        gnm = GraphNodeModule(layer, parameters, [])
        output_shape = gnm.output_shape

        self.assertEqual((10, 26, 26), output_shape)

    def test_input_shape_2d_output_shape_dense(self):
        # mnist input shape (X, 1, 28, 28)
        parameters = {
            'convolution_size_multiplier': 10,
            'dense_size_multiplier': 100,
            'dataset': 'mnist'
        }

        # should be dense
        layer = Layer(1, 0.0)
        layer.add_extra(1.0)
        layer.mark_entry_layer()

        gnm = GraphNodeModule(layer, parameters, [])
        output_shape = gnm.output_shape

        self.assertEqual((100.0,), output_shape)


class TrainerAwareFitnessTest(unittest.TestCase):

    def test_deepcopy(self):
        TrainerAwareFitness.weights = (1.0,)
        fitness = TrainerAwareFitness()

        self.assertEqual(fitness, deepcopy(fitness))

        fitness.train_sign = 123
        self.assertEqual(fitness, deepcopy(fitness))

    def test_setting_train_sign(self):
        TrainerAwareFitness.weights = (1.0,)
        fitness = TrainerAwareFitness()
        self.assertIs(fitness.train_sign, None)

        fitness.train_sign = 1
        self.assertEqual(fitness.train_sign, 1)

        del fitness.train_sign
        self.assertIs(fitness.train_sign, None)

    def test_hash_includes_train_sign(self):
        TrainerAwareFitness.weights = (1.0,)
        fitness1 = TrainerAwareFitness(values=(1,))
        fitness2 = TrainerAwareFitness(values=(1,), train_sign=1)

        hash_1 = hash(fitness1)
        hash_2 = hash(fitness2)

        self.assertNotEqual(hash_1, hash_2)

    def test_can_compare_if_same_trainer_signatures(self):

        TrainerAwareFitness.weights = (1.0,)
        fitness1 = TrainerAwareFitness(values=(1,), train_sign=1)
        fitness2 = TrainerAwareFitness(values=(2,), train_sign=1)

        try:
            fitness1 == fitness2
            fitness1 != fitness2
            fitness1 >= fitness2
            fitness1 <= fitness2
            fitness1 < fitness2
            fitness1 > fitness2
        except ValueError:
            self.fail("Not expecting any exceptions!")

    def test_cant_compare_if_different_trainer_signatures(self):
        TrainerAwareFitness.weights = (1.0,)
        fitness1 = TrainerAwareFitness(values=(1,), train_sign=1)
        fitness2 = TrainerAwareFitness(values=(2,), train_sign=2)

        try:
            fitness1 == fitness2
            self.fail("Expecting an exception!")
        except ValueError:
            pass

        try:
            fitness1 != fitness2
            self.fail("Expecting an exception!")
        except ValueError:
            pass

        try:
            fitness1 >= fitness2
            self.fail("Expecting an exception!")
        except ValueError:
            pass

        try:
            fitness1 <= fitness2
            self.fail("Expecting an exception!")
        except ValueError:
            pass

        try:
            fitness1 < fitness2
            self.fail("Expecting an exception!")
        except ValueError:
            pass

        try:
            fitness1 > fitness2
            self.fail("Expecting an exception!")
        except ValueError:
            pass


class TrainersPopulationSignatureTest(unittest.TestCase):

    def test_populations_with_same_trainers_in_different_order(self):

        trainers_1 = [[1.0, 2.0, 3.0, 4.0], [5.0, 6.0, 7.0, 8.0]]
        trainers_2 = [[5.0, 6.0, 7.0, 8.0], [1.0, 2.0, 3.0, 4.0]]

        signature_1 = signature(trainers_1)
        signature_2 = signature(trainers_2)

        self.assertEqual(signature_1, signature_2)

    def test_different_trainers_have_different_signatures(self):

        trainers_1 = [[1.0, 2.0, 3.0, 4.0]]
        trainers_2 = [[1.0, 2.0, 3.0, 5.0]]

        signature_1 = signature(trainers_1)
        signature_2 = signature(trainers_2)

        self.assertNotEqual(signature_1, signature_2)

    def test_same_trainers_have_same_signatures(self):

        trainers_1 = [[1.0, 2.0, 3.0, 4.0]]
        trainers_2 = [[1.0, 2.0, 3.0, 4.0]]

        signature_1 = signature(trainers_1)
        signature_2 = signature(trainers_2)

        self.assertEqual(signature_1, signature_2)


class CoEvolutionTest(unittest.TestCase):

    def test_coevolution_smoke(self):

        def eval_on_trainer(ind, pred, config):
            val = (sum(ind) * (1.0 / (len(pred) + 1)), 1, 2, 3, 4, 5)
            return MemoryValuePromise(val)

        configuration = DNNEvoConfiguration.from_dict({
            "fp_individual_size": "4",
            "fp_population_size": "4",
            "fp_train_iterations": "1",
            "main_individual_size": "4",
            "main_population_size": "4",
            "main_train_iterations": "1",
            "stale_iterations_cnt": "3",
        })

        evolution = CoEvolution(
            "test",
            eval_on_trainer,
            configuration
        )

        evolution.evolve()
        evolution.evolve()


class TargetDiffValuePromiseTest(unittest.TestCase):

    def test_diffs_target_with_abs(self):
        base_promise = MemoryValuePromise((1.0, ))
        target = -1.0

        target_diff_promise = TargetDiffValuePromise(
            base_promise,
            target
        )
        target_diff = target_diff_promise.get_value()

        self.assertEqual(2.0, target_diff[0])

    def test_diffs_target(self):
        base_promise = MemoryValuePromise((1.0, ))
        target = 3.0

        target_diff_promise = TargetDiffValuePromise(
            base_promise,
            target
        )
        target_diff = target_diff_promise.get_value()

        self.assertEqual(2.0, target_diff[0])

    def test_diffs_tuple(self):
        base_promise = MemoryValuePromise((1.0, 2.0, 3.0))
        target = (2, 4, 6)

        target_diff_promise = TargetDiffValuePromise(
            base_promise,
            target
        )
        target_diff = target_diff_promise.get_value()

        self.assertEqual( (1, 2, 3), target_diff)


class AvgCompositeValuePromiseTest(unittest.TestCase):

    def test_computes_average_of_tuples(self):
        vp1 = MemoryValuePromise((1, 2, 3))
        vp2 = MemoryValuePromise((2, 3, 4))
        vp3 = MemoryValuePromise((3, 4, 5))

        avg_promise = AvgCompositeValuePromise([vp1, vp2, vp3])

        value = avg_promise.get_value()

        self.assertEqual((2.0, 3.0, 4.0), value)

    def test_computes_average(self):
        vp1 = MemoryValuePromise((1,))
        vp2 = MemoryValuePromise((2,))
        vp3 = MemoryValuePromise((3,))

        avg_promise = AvgCompositeValuePromise([vp1, vp2, vp3])

        value = avg_promise.get_value()

        self.assertEqual(2.0, value[0])


class DNNEvoConfigurationTest(unittest.TestCase):

    def test_deserialize(self):
        deserialized_changed = DNNEvoConfiguration.from_dict({
            'main_population_size': '12345'
        })

        self.assertNotEqual(
            DNNEvoConfiguration.main_population_size,
            deserialized_changed.main_population_size
        )

    def test_smoke(self):
        self.assertEqual(
            DNNEvoConfiguration(),
            DNNEvoConfiguration.from_dict(DNNEvoConfiguration().to_dict())
        )


class EvaluatorTest(unittest.TestCase):

    def test_evaluator_throws_when_attempt_to_evaluate_without_trainers(self):

        def eval_on_trainer(ind, pred, config):
            val = (sum(ind), 1, 2, 3, 4, 5,)
            return MemoryValuePromise(val)

        random.seed(1)

        evaluator = Evaluator(
            eval_on_trainer,
            {}
        )

        individual = []

        try:
            evaluator.evaluate(individual)
            self.fail("Expecting an exception to be thrown!")
        except ValueError:
            pass


class PopulationEvolutionTest(unittest.TestCase):

    def test_evolution_grows(self):

        def eval_on_trainer(ind, pred, config):
            val = (sum(ind), 1, 2, 3, 4, 5,)
            return MemoryValuePromise(val)

        random.seed(1)
        toolbox_builder = ToolboxBuilder(DNNEvoConfiguration())
        main_toolbox, fp_toolbox = toolbox_builder.build()
        main_toolbox.unregister("select")
        main_toolbox.register("select", tools.selBest)
        evaluator = Evaluator(
            eval_on_trainer,
            {}
        )
        evaluator.trainers.append([])
        evolution = PopulationEvolution(
            "test",
            main_toolbox,
            evaluator
        )

        for i in range(100):
            evolution.evolve()

        # the max should be monotonically increasing
        maxes = evolution.logbook.select("max")
        for i in range(1, len(maxes)):
            # print("i: " + str(i) + " " + str(maxes[i-1]) + " <= "\
            # + str(maxes[i]))
            self.assertTrue(maxes[i-1] <= maxes[i])


class IntermediateRepresentationTest(unittest.TestCase):

    def test_populate_dependencies_single_node(self):
        layer = Layer(1, 0.0)

        populate_dependencies(layer)

        self.assertEqual(0, len(layer.dependencies))

    def test_populate_dependencies_two_nodes(self):
        layer1 = Layer(1, 0.0)
        layer2 = Layer(2, 0.0)
        layer1.add_neighbor(layer2)

        populate_dependencies(layer1)

        self.assertEqual(1, len(layer2.dependencies))
        self.assertEqual(layer1, layer2.dependencies[0])

    def test_populate_dependencies_three_nodes(self):
        layer1 = Layer(1, 0.0)
        layer2 = Layer(2, 0.0)
        layer3 = Layer(3, 0.0)
        layer1.add_neighbor(layer2)
        layer2.add_neighbor(layer3)

        populate_dependencies(layer1)

        self.assertEqual(1, len(layer2.dependencies))
        self.assertEqual(layer1, layer2.dependencies[0])
        self.assertEqual(1, len(layer3.dependencies))
        self.assertEqual(layer2, layer3.dependencies[0])

    def test_populate_dependencies_two_routes_four_nodes(self):
        layer1 = Layer(1, 0.0)
        layer2 = Layer(2, 0.0)
        layer3 = Layer(3, 0.0)
        layer4 = Layer(4, 0.0)
        layer1.add_neighbor(layer2)
        layer2.add_neighbor(layer3, layer4)
        layer3.add_neighbor(layer4)

        populate_dependencies(layer1)

        self.assertEqual(1, len(layer2.dependencies))
        self.assertEqual(layer1, layer2.dependencies[0])
        self.assertEqual(1, len(layer3.dependencies))
        self.assertEqual(layer2, layer3.dependencies[0])
        self.assertEqual(2, len(layer4.dependencies))
        self.assertTrue(layer2 in layer4.dependencies)
        self.assertTrue(layer3 in layer4.dependencies)

    def test_always_two_layers(self):
        code = [1] * NUMBER_OF_FIELDS
        genotype = Genotype(code)
        layers = genotype_to_layers(genotype)

        self.assertEqual(len(set(layers)), 2)

    def test_only_outputs_two_layer(self):
        code = [
            0.99, 0.99, 0.0, 10.0, 0.0,
            0.5, 0.5, 0.0, 10.0, 0.0,
            0.15, 0.15, 0.0, 10.0, 0.0,
            0.35, 0.35, 0.0, 10.0, 0.0
        ]
        layers = genotype_to_layers(Genotype(code))

        self.assertEqual(len(set(layers)), 2)

    def test_only_inputs_two_layers(self):
        code = [
            0.99, 0.99, 1.0, 10.0, 0.0,
            0.5, 0.5, 1.0, 10.0, 0.0,
            0.15, 0.15, 1.0, 10.0, 0.0,
            0.35, 0.35, 1.0, 10.0, 0.0
        ]
        layers = genotype_to_layers(Genotype(code))

        self.assertEqual(len(set(layers)), 2)

    def test_one_intermediate_layer(self):
        code = [
            0.99, 0.99, 1.0, 100.0, 0.0,  # intput
            0.80, 0.80, 0.0, 100.0, 0.0   # output
        ]

        layers = genotype_to_layers(Genotype(code))

        self.assertEqual(len(set(layers)), 3)


class NNTest(unittest.TestCase):
    dense_layer = layer_type_num(Layer.DENSE)

    def test_exit_node_closest_gene_is_below_threshold(self):
        genotype = [
            0.1333765322417899, 0.323839816760335,
            0.8248049455874152, 0.3560099828135477,
            0.0,

            0.04668414430150292, 0.5253881808552567,
            0.38805416110151714, 0.9017705816098132,
            0.0,

            0.27744017088204453, 0.29967024333082537,
            0.4770922756061522, 0.5563546919450066,
            0.0,

            0.5128697318533849, 0.456011796458229,
            0.35702321575958884, 0.38305056476998145,
            0.0
        ]

        config = {
            'batch_size': '64',
            'dataset': 'mnist',
            'learning_rate': '0.1',
            'momentum': '0.0',
            'optimizer': 'adam',
            'rng_seed': '-1',
            'dense_size_multiplier': '100',
            'train_iterations': '12'
        }

        try:
            model = translate_to_model(
                genotype,
                config
            )
        except Exception:
            self.fail("Expecting no exceptions! :)")

        print(model)

    def test_creates_network_with_2_outputs_instead_of_10(self):
        genotype = [
            0.05643297232680955, 0.31987079048344,
            0.03762880210185737, 0.10017203666178953,
            0.0,

            0.9055363548788498, 0.17268965073833442,
            0.6436491183396985, 0.03427851631634571,
            0.0,

            0.9412644736571887, 0.7378648589338472,
            0.43819571267003954, 0.9507942029769404,
            0.0,

            0.2171261076689831, 0.25102037270192257,
            0.6815583766266253, 0.5273408034905142,
            0.0,

            0.8863164845878119, 0.9359975249240167,
            0.9855625903445477, 0.7589275001664016,
            0.0,

            0.5258778307962373, 0.5300300520960575,
            0.8957028206546487, 0.39895249270357624,
            0.0,

            0.3324311539583331, 0.5644568070893734,
            0.2706152967477192, 0.6512255675016059,
            0.0,

            0.538243532727374, 0.539000522717486,
            0.7618388109208875, 0.4234401800195271,
            0.0,

            0.2509397756492806, 0.8435094743631028,
            0.7427528620358593, 0.8940064352885473,
            0.0,

            0.25335824767348536, 0.4303327881203256,
            0.9299458331631111, 0.9803117977433854,
            0.0,

            0.11265662826049327, 0.6580491935790346,
            0.5360537717659363, 0.3061463712006891,
            0.0,

            0.9399343911920626, 0.13338979294330655,
            0.12424913245847546, 0.3162417529496202,
            0.0,

            0.6514314737069925, 0.9876931345810797,
            0.7110109869462746, 0.4909497626953714,
            0.0,

            0.8159724724147841, 0.34320491913607676,
            0.06187833429953593, 0.9200034587542372,
            0.0,

            0.7951594971127298, 0.6650726892791267,
            0.9427257528790471, 0.887062536967623,
            0.0,

            0.4786095852653405, 0.5444489039736452,
            0.9508059569469689, 0.5942036572043666,
            0.0
        ]

        config = {
            "batch_size": "64",
            "dataset": "mnist",
            "learning_rate": "0.1",
            "momentum": "0.0",
            "dense_size_multiplier": "1",
            "convolution_size_multiplier": "1",
            "stale_iterations_cnt": "5"
        }

        model = translate_to_model(
            genotype,
            config
        )

        print(model)

        exit_node = model.exit_node
        output_size = exit_node.output_size()
        self.assertEqual(output_size, 10)

    def test_initial_layer_with_a_single_gene(self):
        try:
            translate_to_model([
                0.5419894770299267,
                0.7037981973406071,
                0.7154249139926955,
                0.22649571728963136,
                0.0,
                0.6314833242424094,
                0.985382783246899,
                0.9314326233664862,
                0.8744622260775571,
                0.0,
                0.33484514994732417,
                0.7432832573007401,
                0.6718560750225345,
                0.5804914462208081,
                0.0,
                0.7647896551951907,
                0.07649446993242026,
                0.31965899911581086,
                0.9033400172942346,
                0.0
            ], DNNTrainConfiguration().to_dict())
        except Exception:
            self.fail("Expecting no exceptions! :)")

    def test_sample_individual_1(self):
        parameters = DNNTrainConfiguration().to_dict()
        genotype = [
            0.35802602285011664,
            0.6946491399028506,
            0.9954324642952438,
            0.5346513198118172,
            0.0,
            0.8294075111785529,
            0.04630514239666328,
            0.7634902125532778,
            0.16952472890333714,
            0.0,
            0.162244751798166,
            0.9355672316184352,
            0.8717047603945779,
            0.8099141816086157,
            0.0,
            0.1605044858305995,
            0.6973274830267816,
            0.8535386603532029,
            0.04612340453754604,
            0.0
        ]

        try:
            translate_to_model(genotype, parameters)
        except Exception:
            self.fail("Expecting no exceptions! :)")

    def test_sample_individual_2(self):
        parameters = DNNTrainConfiguration().to_dict()
        genotype = [
            0.5419894770299267,
            0.7037981973406071,
            0.7154249139926955,
            0.22649571728963136,
            0.0,
            0.6314833242424094,
            0.985382783246899,
            0.9314326233664862,
            0.8744622260775571,
            0.0,
            0.33484514994732417,
            0.7432832573007401,
            0.6718560750225345,
            0.5804914462208081,
            0.0,
            0.7647896551951907,
            0.07649446993242026,
            0.31965899911581086,
            0.9033400172942346,
            0.0
        ]

        try:
            translate_to_model(genotype, parameters)
        except Exception:
            self.fail("Expecting no exceptions! :)")

    def test_remove_unnecessary_connections(self):
        entry = Layer(0, NNTest.dense_layer)
        l1 = Layer(1, NNTest.dense_layer)
        l2 = Layer(2, NNTest.dense_layer)
        l3 = Layer(3, NNTest.dense_layer)

        entry.add_neighbor(l1)
        l1.add_neighbor(entry, l2, l3)
        l2.add_neighbor(l1, l3)

        remove_unnecessary_connections(entry)

        self.assertEqual(1, len(entry.neighbors))
        self.assertTrue(l1 in entry.neighbors)

        self.assertEqual(2, len(l1.neighbors))
        self.assertTrue(l2 in l1.neighbors)
        self.assertTrue(l3 in l1.neighbors)

        self.assertEqual(1, len(l2.neighbors))
        self.assertTrue(l3 in l2.neighbors)

        self.assertEqual(0, len(l3.neighbors))

    def test_smoke_model_evaluation(self):
        layer1 = Layer(0, NNTest.dense_layer)
        layer1.add_extra(0.5)
        layer1.mark_entry_layer()
        layer1.mark_exit_layer()

        remove_unnecessary_connections(layer1)
        populate_dependencies(layer1)

        parameters = {
            "dense_size_multiplier": 100,
            "batch_size": 128,
            "train_iterations": 1,
            "learning_rate": 0.1,
            "momentum": 0.0,
            "dataset": 'mnist'
        }

        module = wrap_with_nn_module(layer1, parameters)

        print(module)

        try:
            evaluate_model_accuracy(module, [], parameters)
        except Exception:
            self.fail("Expecting no exceptions! :)")


class LayerTest(unittest.TestCase):

    def test_layer_type_first_bucket(self):
        layer_type = 1.0 / (2.0 * len(Layer.types))
        layer = Layer(1, layer_type)

        self.assertEqual(layer.layer_type, Layer.types[0])

    def test_layer_type_second_bucket(self):
        layer_type = 1.0 / len(Layer.types) + 1.0 / (2.0 * len(Layer.types))
        layer = Layer(1, layer_type)

        self.assertEqual(layer.layer_type, Layer.types[1])

    def test_layer_type_beginning_of_second_bucket(self):
        layer_type = 1.0 / len(Layer.types)
        layer = Layer(1, layer_type)

        self.assertEqual(layer.layer_type, Layer.types[1])

    def test_layer_type_0_0(self):
        layer_type = 0.0
        layer = Layer(1, layer_type)

        self.assertEqual(layer.layer_type, Layer.types[0])

    def test_layer_type_1_0(self):
        layer_type = 1.0
        layer = Layer(1, layer_type)

        self.assertEqual(layer.layer_type, Layer.types[-1])

    def test_id(self):
        layer1 = Layer(1, 0.0)
        layer2 = Layer(2, 0.0)

        self.assertEqual(1, layer1.id)
        self.assertEqual(2, layer2.id)
        self.assertNotEqual(layer1.id, layer2.id)

    def test_layers_in_set(self):
        layer1 = Layer(1, 0.0)
        layer2 = Layer(1, 0.0)

        deduped = set([layer1, layer2])

        self.assertEqual(1, len(deduped))


class GeneTest(unittest.TestCase):

    def test_is_output(self):
        gene = Gene(1, 2, 0.25, 100, 0.0)
        self.assertTrue(gene.is_output())
        self.assertFalse(gene.is_input())

    def test_is_input(self):
        gene = Gene(1, 2, 0.75, 100, 0.0)
        self.assertFalse(gene.is_output())
        self.assertTrue(gene.is_input())


class GenotypeTest(unittest.TestCase):

    def test_first_gene_always_0_0(self):
        genotype = Genotype([])
        gene = genotype.gene(0)

        self.assertEqual(gene.x, 0.0)
        self.assertEqual(gene.y, 0.0)
        self.assertEqual(gene.gene_type, Gene.OUTPUT)
        self.assertNotEqual(gene.extra, 0)

    def test_last_gene_always_1_1(self):
        genotype = Genotype([])
        gene = genotype.gene(1)

        self.assertEqual(gene.x, 1.0)
        self.assertEqual(gene.y, 1.0)
        self.assertEqual(gene.gene_type, Gene.INPUT)
        self.assertNotEqual(gene.extra, 0)

    def test_always_more_than_two_genes(self):
        genotype = Genotype([])
        self.assertEqual(2, genotype.size)

    def test_gene_type_input(self):
        code = [2, 3, 0.75, 100]
        genotype = Genotype(code)
        gene = genotype.gene(1)

        self.assertEqual(gene.x, 2)
        self.assertEqual(gene.y, 3)
        self.assertEqual(gene.gene_type, Gene.INPUT)
        self.assertEqual(gene.extra, 100)

    def test_increment_id(self):
        genotype1 = Genotype.random(1)
        genotype2 = Genotype.random(1)

        self.assertNotEqual(
            genotype1.id,
            genotype2.id
        )
        self.assertEqual(genotype1.id+1, genotype2.id)
