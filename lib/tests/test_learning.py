from torchx.evo.intermediate import Layer
from torchx.evo.nn import (
    remove_unnecessary_connections,
    populate_dependencies,
    evaluate_model_accuracy,
    wrap_with_nn_module,
    GraphModule
)

import torch.nn.functional as F
import torch.nn as nn
import unittest


class MNISTTest(unittest.TestCase):

    parameters = {
        "dataset": "mnist",
        "size_multiplier": 100,
        "batch_size": 64,
        "train_iterations": 25,
        "learning_rate": 0.1,
        "momentum": 0.00,
        "activation_fn": 'tanh'
    }

    def test_cirsean_2500(self):

        layer1 = Layer(0)
        layer1.add_extra(25)
        layer2 = Layer(1)
        layer2.add_extra(20)
        layer1.add_neighbor(layer2)
        layer3 = Layer(2)
        layer3.add_extra(15)
        layer2.add_neighbor(layer3)
        layer4 = Layer(3)
        layer4.add_extra(10)
        layer3.add_neighbor(layer4)
        layer5 = Layer(4)
        layer5.add_extra(5)
        layer4.add_neighbor(layer5)
        layer6 = Layer(5)
        layer6.add_extra(5)
        layer5.add_neighbor(layer6)

        params = MNISTTest.parameters.copy()
        params['activation_fn'] = 'tanh'

        remove_unnecessary_connections(layer1)
        populate_dependencies(layer1)

        graph_node_module = wrap_with_nn_module(layer6, params)
        gm = GraphModule(graph_node_module, params)
        accuracy = evaluate_model_accuracy(gm, [], params)

        self.assertGreater(accuracy, 87.0)

    def test_lenet(self):

        class Net(nn.Module):
            def __init__(self):
                super(Net, self).__init__()
                self.conv1 = nn.Conv2d(1, 10, kernel_size=5)
                self.conv2 = nn.Conv2d(10, 20, kernel_size=5)
                self.conv2_drop = nn.Dropout2d()
                self.fc1 = nn.Linear(320, 50)
                self.fc2 = nn.Linear(50, 10)

            def forward(self, x):
                x = F.relu(F.max_pool2d(self.conv1(x), 2))
                x = F.relu(F.max_pool2d(self.conv2_drop(self.conv2(x)), 2))
                x = x.view(-1, 320)
                x = F.relu(self.fc1(x))
                x = F.dropout(x, training=self.training)
                x = self.fc2(x)
                return F.log_softmax(x, dim=1)

        params = MNISTTest.parameters.copy()
        params['train_iterations'] = 10

        accuracy = evaluate_model_accuracy(Net(), [], params)

        self.assertGreater(accuracy, 97)


class CIFAR10Test(unittest.TestCase):

    parameters = {
        "dataset": "cifar10",
        "size_multiplier": 100,
        "input_size": 3*32*32,
        "output_size": 10,
        "batch_size": 64,
        "train_iterations": 5,
        "learning_rate": 0.1,
        "momentum": 0.00,
        "activation_fn": 'relu'
    }

    def test_lenet(self):

        class Net(nn.Module):
            def __init__(self):
                super(Net, self).__init__()
                self.conv1 = nn.Conv2d(3, 6, 5)
                self.pool = nn.MaxPool2d(2, 2)
                self.conv2 = nn.Conv2d(6, 16, 5)
                self.fc1 = nn.Linear(16 * 5 * 5, 120)
                self.fc2 = nn.Linear(120, 84)
                self.fc3 = nn.Linear(84, 10)

            def forward(self, x):
                x = self.pool(F.relu(self.conv1(x)))
                x = self.pool(F.relu(self.conv2(x)))
                x = x.view(-1, 16 * 5 * 5)
                x = F.relu(self.fc1(x))
                x = F.relu(self.fc2(x))
                x = self.fc3(x)
                return F.log_softmax(x, dim=1)

        params = CIFAR10Test.parameters.copy()

        accuracy = evaluate_model_accuracy(Net(), [], params)

        self.assertGreater(accuracy, 50)
