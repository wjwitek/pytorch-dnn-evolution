from typing import List

import torch
import random

Code = List[float]

NUMBER_OF_FIELDS = 5
OUTPUT_TYPE_THRESHOLD = 0.5

genotype_id = 0


def get_id():
    global genotype_id
    genotype_id += 1
    return genotype_id


def genes_distance_matrix(genotype):
    xs = torch.Tensor(
        [genotype.gene(i).x for i in range(genotype.size)]
    )
    ys = torch.Tensor(
        [genotype.gene(i).y for i in range(genotype.size)]
    )

    length = xs.size(0)

    x = xs.expand(length, length)
    y = ys.expand(length, length)
    x_t = torch.t(x)
    y_t = torch.t(y)

    distances = torch.sqrt((x - x_t) * (x - x_t) + (y - y_t) * (y - y_t))

    # ensure in first row we always have a at least one zero (apart from the
    # first column which is the distance between the point itself :))
    _, min_idx = torch.min(distances[0][1:], 0)
    min_idx = min_idx.item()    # extract the value from tensor - unpack
    min_idx += 1                # add +1 to compensate cutting out first column
                                # earlier

    # make sure that there is at least one connection from the entry layer
    distances[0][min_idx] = 0.0

    # make sure that there is at least one connection to the exit layer
    # the [:-1, -1] syntax is for selecting the last column of the tensor
    # without the last row
    _, min_idx = torch.min(distances[:-1, -1], 0)
    min_idx = min_idx.item()
    distances[min_idx][-1] = 0.0

    # make sure that the last layer gets disconnected from everything
    # 1.5 - the value is higher than sqrt(2) so it will always be disconnected)
    distances[length-1] = torch.Tensor([1.5] * genotype.size)

    return distances


def cut_off_distance(distance_matrix):
    """
        Average distance in the matrix
    """
    return torch.sum(distance_matrix) / distance_matrix.nelement()


def genes_adjacency_matrix(genotype):
    distance_matrix = genes_distance_matrix(genotype)
    cut_off_threshold = cut_off_distance(distance_matrix)

    # = required for edge cases like having only two layers
    return distance_matrix <= cut_off_threshold


class Gene(object):
    """
        There are two types of genes - INPUT and OUTPUT ones.
        INPUT - denotes input to a layer.
        OUTPUT - denotes output from a layer
    """
    INPUT = 1
    OUTPUT = 0

    def __init__(
            self,
            x,
            y,
            gene_type_value,
            extra,
            layer_type
            ):
        self.__x = x
        self.__y = y
        self.__extra = extra
        self.__gene_type_value = gene_type_value
        self.__gene_type = Gene.OUTPUT \
            if gene_type_value < OUTPUT_TYPE_THRESHOLD\
            else Gene.INPUT
        self.__layer_type = layer_type

    @property
    def layer_type(self):
        return self.__layer_type

    @property
    def x(self):
        return self.__x

    @property
    def y(self):
        return self.__y

    @property
    def type_value(self):
        return self.__gene_type_value

    @property
    def extra(self):
        return self.__extra

    @property
    def gene_type(self):
        return self.__gene_type

    def is_output(self):
        return self.__gene_type == Gene.OUTPUT

    def is_input(self):
        return self.__gene_type == Gene.INPUT


def random_floats(size):
    return [random.random() for _ in range(size)]


class Genotype(object):

    ENTRY_POINT = [0.0, 0.0, OUTPUT_TYPE_THRESHOLD-0.01, 0.0, 0.0]
    EXIT_POINT = [1.0, 1.0, OUTPUT_TYPE_THRESHOLD+0.01, 0.0, 0.0]

    def __init__(
            self,
            code: Code,
            ):
        self.__id = get_id()
        self.__code = self.__entry_point() + code + self.__exit_point()

    def __entry_point(self):
        copy = Genotype.ENTRY_POINT[:]
        # set random extra data - so in edge cases we don't end up with a layer
        # with no outputs
        copy[3] = random.random()

        # set random layer type
        copy[4] = random.random()
        return copy

    def __exit_point(self):
        copy = Genotype.EXIT_POINT[:]
        # set random extra data - so in edge cases we don't end up with a layer
        # with no outputs
        copy[3] = random.random()

        # exit layer should always be dense
        copy[4] = 0.0
        return copy

    def gene(self, idx):
        return Gene(
            self.__code[idx * NUMBER_OF_FIELDS],
            self.__code[idx * NUMBER_OF_FIELDS + 1],
            self.__code[idx * NUMBER_OF_FIELDS + 2],
            self.__code[idx * NUMBER_OF_FIELDS + 3],
            self.__code[idx * NUMBER_OF_FIELDS + 4],
        )

    @property
    def size(self):
        return int(self.raw_size / NUMBER_OF_FIELDS)

    @property
    def raw_size(self):
        return len(self.__code)

    @property
    def code(self):
        return self.__code

    @property
    def id(self):
        return self.__id

    @staticmethod
    def random(size):
        code = random_floats(size)
        return Genotype(code=code)

    def clone(self, new_raw_code):
        return Genotype(new_raw_code)
