import logging
import random
import json
from collections import deque
from dataclasses import dataclass, field, InitVar
from typing import Deque, Callable, TypeVar, Generic, List

import numpy
from deap import base, tools

from torchx.evo.evolution.configuration import ConfigurationDict
from torchx.evo.evolution.toolbox import (
    TrainerAwareFitness,
    Individual,
    Population,
    signature,
)
from torchx.evo.utils.iteration_tracker import StatefulIterationTracker
from torchx.evo.utils.promises import AvgCompositeValuePromise, ValuePromise

logger = logging.getLogger('evolution')

Trainer = TypeVar('Trainer')
EvaluateFunc = Callable[[Individual, Trainer, ConfigurationDict], ValuePromise[float]]


@dataclass
class Evaluator(Generic[Trainer]):
    evaluate_with_trainer: EvaluateFunc[Trainer]
    evaluation_parameters: ConfigurationDict
    max_trainers: InitVar[int] = 1

    trainers: Deque[Trainer] = field(init=False)

    def __post_init__(self, max_trainers: int):
        self.trainers = deque(maxlen=max_trainers)

    def evaluate(self, individual: Individual) -> ValuePromise[float]:
        if not self.trainers:
            raise ValueError("Evaluator: no trainers have been added! Can't evaluate!")

        return AvgCompositeValuePromise([
            self.evaluate_with_trainer(
                individual,
                trainer,
                self.evaluation_parameters
            )
            for trainer in self.trainers
        ])


class PopulationEvolution(Generic[Trainer]):
    population_name: str
    toolbox: base.Toolbox
    evaluator: Evaluator[Trainer]
    population: Population
    iteration: StatefulIterationTracker
    history: tools.History
    logbook: tools.Logbook
    stats: tools.Statistics

    def __init__(
            self,
            population_name: str,
            toolbox: base.Toolbox,
            evaluator: Evaluator[Trainer],
            individuals: List,
            iteration_no: int,
            current_target: int = None
    ):
        self.population_name = population_name
        self.toolbox = toolbox
        self.evaluator = evaluator
        logger.debug('Loading population ')
        self.population = toolbox.population()
        logger.debug(f'Population created: {self.population}')
        if individuals is not None:
            logger.debug('appending old population data ' + str(individuals))
            for (population_individual, stored_individual) in zip(self.population, individuals):
                population_individual.clear()
                population_individual.extend(json.loads(stored_individual.genotype))

        self._log = logger.getChild(self.population_name)

        # in the first iteration we have to evaluate most probably x2 population length
        initial_target = current_target if current_target is not None else len(self.population) * 2
        self.iteration = StatefulIterationTracker(log=self._log,
                                                  initial_no=iteration_no,
                                                  initial_target=initial_target)
        logger.debug('StatefulIterationTracker loaded ')

        self.logbook = tools.Logbook()
        self.logbook.log_header = False
        self.logbook.header = ("gen", "max", "min", "avg", "std")
        logger.debug('Logbook loaded ')

        self.stats = tools.Statistics(key=lambda ind: ind.fitness.values)
        self.stats.register("avg", numpy.mean)
        self.stats.register("std", numpy.std)
        self.stats.register("min", numpy.min)
        self.stats.register("max", numpy.max)
        logger.debug('Statistics loaded ')

        self.history = tools.History()
        self.toolbox.decorate("mate", self.history.decorator)
        self.toolbox.decorate("mutate", self.history.decorator)
        self.history.update(self.population)
        logger.debug('PopulationEvolution created')

    def fitness_history(self):
        """
        Returns the statistics history as a list of lists of values. The order
        is the same as in logbook header
        """
        values = [
            list(history)
            for history
            in self.logbook.select(*self.logbook.header)
        ]
        labels = list(self.logbook.header)

        return {
            'values': values,
            'labels': labels
        }

    def last_fitness_update(self):
        labels = list(self.logbook.header)

        last_update_as_dict = self.logbook[-1]

        # every element is wrapped in a list to make the format look
        # the same as in the fitness_history() call
        # this makes implementing client using this data easier
        values = [
            [
                last_update_as_dict[label]
            ]
            for label in labels
        ]

        return {
            'values': values,
            'labels': labels,
        }

    def max_fitness(self, last_n):
        return self.logbook.select("max")[-last_n:]

    def best(self):
        # if current iteration number == 0 evolve() was not called yet - we need to pick random
        if self.iteration.current_no == 0:
            idx = random.randint(0, len(self.population) - 1)
            best_individual = self.population[idx]
        else:
            best_individual = tools.selBest(self.population, 1)[0]

        best_clone = self.toolbox.clone(best_individual)
        return best_clone

    def evolve(self):
        """Implementation based on
        https://github.com/DEAP/deap/blob/master/deap/algorithms.py ->
        eaSimple"""

        with self.iteration.enter() as iteration_section:
            offspring: Population = [self.toolbox.clone(ind) for ind in self.population]

            # Apply crossover and mutation on the offspring
            for i in range(1, len(offspring), 2):
                if random.random() < self.toolbox.cx_probability():
                    offspring[i - 1], offspring[i] = self.toolbox.mate(offspring[i - 1], offspring[i])
                    del offspring[i - 1].fitness.values
                    del offspring[i].fitness.values
                    del offspring[i - 1].fitness.train_sign
                    del offspring[i].fitness.train_sign

            for i in range(len(offspring)):
                if random.random() < self.toolbox.mut_probability():
                    offspring[i], = self.toolbox.mutate(offspring[i])
                    del offspring[i].fitness.values
                    del offspring[i].fitness.train_sign

            all_individuals = self.population + offspring

            train_sign = signature(self.evaluator.trainers)
            to_evaluate = [
                ind
                for ind in all_individuals
                if not ind.fitness.valid or ind.fitness.train_sign != train_sign
            ]
            iteration_section.set_target(len(to_evaluate))

            if not to_evaluate:
                self._log.debug("Nothing to evaluate - moving forward...")
            else:
                promises = self.toolbox.map(self.evaluator.evaluate, to_evaluate)

                def retrieve_value(promise):
                    value = promise.get_value()
                    iteration_section.tick()
                    return value

                fitnesses = self.toolbox.map(retrieve_value, promises)

                self._log.debug(f"Using trainers: {self.evaluator.trainers}")
                self._log.debug(f"Using trainers: {train_sign}")

                for ind, fit in zip(to_evaluate, fitnesses):
                    # use the first value as fitness and turn it into a tuple
                    ind.fitness.values = fit
                    ind.fitness.train_sign = train_sign
                    self._log.debug(f"Evaluated: {ind} Fitness: {ind.fitness}")

            self.population = self.toolbox.select(all_individuals, len(self.population))

            self._log.debug(f"Population length after selection: {len(self.population)}")
            self._log.debug(f"First fitness: {self.population[0].fitness}")
            record = self.stats.compile(self.population)
            self.logbook.record(gen=self.iteration.current_no, **record)
