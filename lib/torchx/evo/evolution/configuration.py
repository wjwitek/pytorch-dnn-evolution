import logging
from abc import ABC
from dataclasses import dataclass, field, asdict, fields
from typing import Dict, TypeVar, Type, Union, List

logger = logging.getLogger('evolution')

K = TypeVar('K', bound='Configuration')

ConfigurationDict = Dict[str, Union[str, 'ConfigurationDict']]


@dataclass
class Configuration(ABC):
    def to_dict(self) -> ConfigurationDict:
        def str_child(child):
            if isinstance(child, dict) or isinstance(child, list):
                return child
            else:
                return str(child)

        def str_dict(items) -> ConfigurationDict:
            return {k: str_child(v) for k, v in items}

        return asdict(self, dict_factory=str_dict)

    @classmethod
    def from_dict(cls: Type[K], as_dict: ConfigurationDict) -> K:
        logger.debug(f"Turning: {as_dict} into: {cls} type: {type(as_dict)}")

        config = cls()
        for prop in fields(cls):
            if prop.name in as_dict and as_dict[prop.name]:
                if issubclass(prop.type, Configuration):
                    value = prop.type.from_dict(as_dict[prop.name])
                else:
                    value = prop.type(as_dict[prop.name])

                setattr(config, prop.name, value)

        logger.debug(f"Turned: {as_dict} into {config}")

        return config


@dataclass
class DNNTrainConfiguration(Configuration):
    optimizer: str = 'sgd'
    rng_seed: int = -1
    batch_size: int = 128
    learning_rate: float = 0.1
    momentum: float = 0.0
    dataset: str = 'mnist'
    dense_size_multiplier: int = 500
    convolution_size_multiplier: int = 5
    train_iterations: int = 12


@dataclass
class DNNEvoConfiguration(Configuration):
    main_train_config: DNNTrainConfiguration = field(default_factory=DNNTrainConfiguration)
    fp_train_config: DNNTrainConfiguration = field(default_factory=DNNTrainConfiguration)

    stale_iterations_cnt: int = 5
    """
    In how many iterations we need to have the same max value
    so new trainer search is executed.
    """

    main_individual_size: int = 64
    main_mutation_probability: float = 0.02
    main_population_size: int = 32
    main_crossover_probability: float = 0.50
    main_population: List = field(default_factory=lambda: [])
    main_target: int = 0

    fp_individual_size: int = 1000
    fp_mutation_probability: float = 0.02
    fp_population_size: int = 8
    fp_crossover_probability: float = 0.50
    fp_population: List = field(default_factory=lambda: [])
    fp_target: int = 0

    start_from: int = 0
