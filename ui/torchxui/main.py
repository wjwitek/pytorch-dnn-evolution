import logging
import os
import sys

from torchxui.db import get_db_session
from torchxui.service import initialize_flask, initialize_socketio
from torchxui.zookeeper import DNNEvoZookeeperClient

logging.basicConfig(
    level=logging.DEBUG,
    format='[%(asctime)s: %(levelname)s/%(name)s] %(message)s',
    handlers=[
        # logging.handlers.RotatingFileHandler(
        #     "torch-dnnevo.log",
        #     maxBytes=(1048576*5),
        #     backupCount=7,
        # ),
        logging.StreamHandler(sys.stdout),
    ])

logger = logging.getLogger(__name__)

DEBUG = os.environ.get('DEBUG', 'FALSE').upper() == 'TRUE'


def main():
    instance_id = os.getenv('INSTANCE_ID', f'client_{os.getpid()}')
    logger.info('Starting zookeeper connection')
    zk_client = DNNEvoZookeeperClient()

    def run_instance():
        logger.info('Instance won the election')

        logger.info('Starting server with socketio')
        db_session = get_db_session()
        app = initialize_flask()
        socketio = initialize_socketio(app, db_session, zk_client)
        logger.info('Started server with socketio')
        socketio.run(app, host='0.0.0.0', port=8080)

    try:
        zk_client.elect_leader(instance_id, run_instance)
    finally:
        zk_client.stop()
        zk_client.close()



if __name__ == '__main__':
    main()
