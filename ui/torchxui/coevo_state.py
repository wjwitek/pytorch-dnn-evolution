import json
import logging
import random
from enum import Enum
from typing import Optional

from torchx.evo.evolution import DNNEvoConfiguration
from torchx.evo.evolution.coevolution import CoEvolution
from torchx.evo.utils.iteration_tracker import IterationStatus
from torchxui.celery import (
    clear_queue,
    remote_evaluate_model,
    set_extra_info,
)
from torchxui.db import (
    Experiment,
    Individual,
    Iteration,
    Population,
    get_experiment,
)

logger = logging.getLogger(__name__)


class Status(Enum):
    NOT_STARTED = 1  # the thread has not been started
    INITIALIZING = 2  # the thread is initializing (before running)
    RUNNING = 3  # the thread is running
    INTERRUPTING = 4  # the thread is being interrupted
    INTERRUPTED = 5  # finished execution after being interrupted
    FINISHED = 6  # finished execution without interrupting
    INTERRUPTED_EX = 7  # finished execution after being interrupted with an exception


class CoEvolutionState(object):
    _coevolution: Optional[CoEvolution]

    def __init__(self, configuration_dict, db_session=None, zk_client=None):
        logger.debug(f'Initializing coevolution loop: {configuration_dict}')
        self._db_session = db_session
        self._zk_client = zk_client
        self._status = Status.INITIALIZING
        self._raw_configuration_dict = configuration_dict
        self._is_running = True
        self._coevolution = None
        self._progress_listeners = []
        self._fitness_listeners = []
        experiment_id = self._get_if_present('experiment-id', None)

        if experiment_id is not None:
            self._load_experiment(experiment_id)
            self._configuration = DNNEvoConfiguration.from_dict(
                json.loads(self.experiment.configuration_json),
            )
            logger.debug(f'Loaded configuration {self._configuration}')
            self._name = self.experiment.name
            self._max_iterations = int(self.experiment.max_iterations)
            self._start_from = int(self.experiment.iteration_no)
        else:
            self._configuration = DNNEvoConfiguration.from_dict(
                json.loads(configuration_dict['evo-config']),
            )
            logger.debug(f'Loaded configuration {self._configuration}')
            self._name = self._get_if_present('experiment-name', 'Test experiment')
            self._max_iterations = int(self._get_if_present('max-iterations', '0'))
            self._start_from = 0
            self._persist_experiment()

    @property
    def _has_db_session(self):
        return self._db_session is not None

    @property
    def _has_zk_client(self):
        return self._zk_client is not None

    def _get_if_present(self, key, default):
        value = self._raw_configuration_dict[key] \
            if self._raw_configuration_dict is not None \
            and key in self._raw_configuration_dict \
            else default
        return value

    def _persist_experiment(self):
        if self._has_db_session:
            self._main_population = Population(name="main", iterations=[])
            self._fp_population = Population(name="fp", iterations=[])
            self.experiment = Experiment(
                name=self.name,
                max_iterations=self._max_iterations,
                iteration_no=self.iteration_no,
                configuration_json=json.dumps(
                    self._configuration.to_dict()
                ),
                populations=[self._main_population, self._fp_population]
            )
            self._db_session.add(self.experiment)
            self._commit_session()
        if self._has_zk_client:
            self._zk_client.set_experiment_id(self.experiment.id)

    def _load_experiment(self, experiment_id):
        self.experiment = get_experiment(self._db_session, experiment_id)
        main_population, fp_population = self.experiment.populations
        self._main_population = main_population
        self._fp_population = fp_population
        if self._has_zk_client:
            self._zk_client.set_experiment_id(self.experiment.id)

    @property
    def name(self):
        return self._name

    @property
    def is_running(self):
        return self._is_running

    @property
    def status(self):
        return self._status

    @status.setter
    def status(self, status):
        self._status = status

    def interrupt(self):
        self.status = Status.INTERRUPTING
        self._zk_client.set_experiment_id('')
        self._is_running = False

        clear_queue()

    def within_iterations_limit(self):
        if self._max_iterations < 1:
            return True
        else:
            return self._max_iterations > self.iteration_no

    @property
    def iteration_progress(self):
        if self._coevolution is not None:
            return self._coevolution.iteration.current_progress

        return 0

    @property
    def individuals_to_evaluate(self):
        if self._coevolution is not None:
            return self._coevolution.iteration.current_target

        return 0

    @property
    def history(self):
        if self._coevolution is not None:
            return self._coevolution.individual_history

        return None

    @property
    def evolution_progress(self):
        if self._max_iterations < 1:
            return -1
        return int((self.iteration_no / self._max_iterations) * 100)

    @property
    def iteration_no(self):
        if self._coevolution is not None:
            return int(self._coevolution.iteration.current_no)

        return 0

    def fitness_history(self):
        if self._coevolution is not None:
            fitness_history = self._coevolution.fitness_history()
            return fitness_history

        return {
            "main": {
                "values": [[], [], [], [], []],
                "labels": ["gen", "max", "min", "avg", "std"]
            },
            "fp": {
                "values": [[], [], [], [], []],
                "labels": ["gen", "max", "min", "avg", "std"]
            }
        }

    def _prepare_population_for_storing(self, population, self_population):
        def extract_history_index(i):
            return i.history_index if hasattr(i, 'history_index') else 0

        if self._has_db_session:
            individuals = list(map(
                lambda i: Individual(
                    fitness=i.fitness.values[0] if i.fitness.valid else 0.0,
                    genotype=json.dumps(list(i)),
                    evaluation_time=0.0,
                    genealogy_index=extract_history_index(i)
                ),
                population
            ))
            iteration = Iteration(
                iteration_no=self.iteration_no,
                individuals=individuals,
                current_target=self._coevolution.iteration.current_target
            )
            self_population.iterations.append(iteration)

    def _persist(self):
        main_population, fp_population = self._coevolution.population()
        self._prepare_population_for_storing(main_population, self._main_population)
        self._prepare_population_for_storing(fp_population, self._fp_population)
        self.experiment.iteration_no = self.iteration_no
        self._commit_session()

    def _commit_session(self):
        if self._has_db_session:
            try:
                self._db_session.commit()
                logger.debug('Committed session')
            except Exception as e:
                logger.error('Exception while committing data')
                logger.error(e)
                self._db_session.rollback()

    def _create_coevolution(self, name, configuration):
        if len(self._main_population.iterations) > 0:
            configuration.main_iteration = self._main_population.iterations[-1].individuals
            configuration.main_target = self._main_population.iterations[-1].current_target
        if len(self._fp_population.iterations) > 0:
            configuration.fp_iteration = self._fp_population.iterations[-1].individuals
            configuration.fp_target = self._fp_population.iterations[-1].current_target
        configuration.start_from = self._start_from
        logger.debug(f'Coevolution configuration: {configuration}')

        return CoEvolution(
            name,
            remote_evaluate_model,
            configuration
        )

    def _fp_top_individuals(self):
        top_ind = []
        for iteration in self._fp_population.iterations:
            best_ind = min(iteration.individuals, key=lambda x: x.fitness)
            top_ind.append(best_ind.id)

        logger.debug(f'FP Top Individuals: {top_ind}')
        return top_ind

    def _main_top_individuals(self):
        top_ind = []
        for iteration in self._main_population.iterations:
            logger.debug(f'MAIN Top Individuals: Iteration: {iteration.id}')
            best_ind = max(iteration.individuals, key=lambda x: x.fitness)
            top_ind.append(best_ind.id)

        logger.debug(f'MAIN Top Individuals: {top_ind}')
        return top_ind

    def top_individuals(self):
        fp = []
        main = []
        if self.iteration_no > 0:
            fp = self._fp_top_individuals()
            main = self._main_top_individuals()

        return {'fp': fp, 'main': main}

    def _initialize_rng(self):
        if self._configuration.main_train_config.rng_seed >= 0:
            random.seed(self._configuration.main_train_config.rng_seed)

    def progress(self):
        return {
            'evolution_progress': self.evolution_progress,
            'iteration_progress': self.iteration_progress,
        }

    def _notify_progress_event(self, progress):
        for listener in self._progress_listeners:
            listener.handle_progress_update(progress)

    def _notify_end_of_iteration(self):
        self._notify_progress_event(self.progress())

    def _notify_fitness_update(self):
        last_fitness_progress = self._coevolution.last_fitness_update()
        for listener in self._fitness_listeners:
            listener.handle_fitness_update(last_fitness_progress)

    def register_progress_listener(self, listener):
        self._progress_listeners.append(listener)

    def register_fitness_listener(self, listener):
        self._fitness_listeners.append(listener)

    def handle_iteration_progress(self, progress: IterationStatus):
        self._notify_progress_event({
            'iteration_no': progress.current_no,
            'iteration_progress': progress.current_progress,
            'evolution_progress': self.evolution_progress,
        })

    def __call__(self):
        logger.debug('Initializing Evolution Thread')
        self._initialize_rng()
        if self._coevolution is None:
            self._coevolution = self._create_coevolution(self._name, self._configuration)
        logger.debug(f'Coevolution created: {self._coevolution}')
        self._coevolution.iteration.add_listener(
            lambda p: self.handle_iteration_progress(p)
        )
        logger.debug('Initialized Evolution Thread')

        self.status = Status.RUNNING
        logger.debug('Status set to RUNNING')
        self._persist()
        while self.is_running and self.within_iterations_limit():
            set_extra_info(f'Iteration: {self._coevolution.iteration.current_no}')
            try:
                self._coevolution.evolve()
            except Exception as e:
                logger.exception("""
                Main evolution thread is being killed because of an exception!
                """)
                self.status = Status.INTERRUPTED_EX
                raise e

            self._persist()
            self._notify_end_of_iteration()
            self._notify_fitness_update()

        if self.status == Status.RUNNING:
            self.status = Status.FINISHED
            logger.debug('Finished Evolution Thread')
            self._zk_client.set_experiment_id('')
        else:
            self.status = Status.INTERRUPTED
            logger.debug('Interrupted Evolution Thread')
