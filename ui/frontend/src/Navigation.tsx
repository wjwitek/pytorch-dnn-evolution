import * as React from 'react';
import { Navbar, Nav, NavItem, } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import paths from './routing';
import { LinkContainer } from 'react-router-bootstrap';

interface NavigationProps {
    notUsed?: string,
}

export const Navigation: React.StatelessComponent<NavigationProps> = (props) => {
    return (
        <Navbar fluid={true} collapseOnSelect={true}>
            <Navbar.Header>
                <Navbar.Brand>
                    <Link to={paths.control}>
                        DNN Evolution Admin
                    </Link>
                </Navbar.Brand>
                <Navbar.Toggle />
            </Navbar.Header>
            <Nav bsStyle="tabs">
                <LinkContainer to={paths.control}>
                    <NavItem>
                        Control
                    </NavItem>
                </LinkContainer>
                <LinkContainer to={paths.current}>
                    <NavItem>
                        Current
                    </NavItem>
                </LinkContainer>
                <LinkContainer to={paths.experiments}>
                    <NavItem>
                        Experiments
                    </NavItem>
                </LinkContainer>
                <LinkContainer to={paths.individuals}>
                    <NavItem>
                        Individuals
                    </NavItem>
                </LinkContainer>
                <LinkContainer to={paths.database}>
                    <NavItem>
                        Database
                    </NavItem>
                </LinkContainer>
            </Nav>
        </Navbar>
    );
};
