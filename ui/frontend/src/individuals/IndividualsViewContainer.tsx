import * as React from "react";
import { IndividualsView } from "./IndividualsView";
import { SocketContextProps } from "../common";
import { withSocketContext } from "../common/SocketContext";
import PaginationContainer from "../pagination/PaginationContainer";

interface State {
  res: any[];
  count: number;
  currentPageNumber: number;
}

export class IndividualsViewContainer extends React.Component<
  SocketContextProps,
  State
> {
  public constructor(props) {
    super(props);
    this.state = { res: [], count: 0, currentPageNumber: 0 };
    this.getNextPage(this.state.currentPageNumber);
    this.props.socket.on("individuals_response", this.handleIndividual);
  }

  public componentWillUnmount() {
    this.props.socket.off("individuals_response", this.handleIndividual);
  }

  public render() {
    return (
      <div>
        <IndividualsView individuals={this.state.res} />
        <PaginationContainer
          currentPageNumber={this.state.currentPageNumber}
          handler={this.getNextPage}
          pageCount={this.state.count / 10}
        />
      </div>
    );
  }

  public getNextPage = (page: number) => {
    this.setState({ currentPageNumber: page });
    this.props.socket.emit("individuals_request", { page });
  }

  private handleIndividual = (message: any) => {
    const msg = JSON.parse(message);
    this.setState({ res: msg.res, count: msg.count });
  }
}

export default withSocketContext(IndividualsViewContainer);
