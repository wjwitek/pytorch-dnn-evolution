import * as React from 'react';
import './pagination.css';
import {Pagination} from "react-bootstrap";


export class PaginationComponent extends React.Component<any, any> {

    private static PAGES_VISIBLE: number = 9;

    public render() {
        return (
            <Pagination>
                <Pagination.Prev onClick={this.handlePrev} value='previous'/>
                {this.preparePaginationButtons()}
                {this.prepareLastButtons()}
                <Pagination.Next onClick={this.handleNext} value='next'/>
            </Pagination>
        );
    }

    private handleClick = (event) => {
        const newCurrentPageNumber = this.prepareNewCurrentBasedOnButtonClicked(
            event.target.innerHTML);
        if (newCurrentPageNumber !== this.props.pageNumber) {
            this.handleCurrentPageChange(newCurrentPageNumber);
        }
    };

    private handlePrev = () => {
        const newCurrentPageNumber = this.props.pageNumber > 1 ? this.props.pageNumber - 1
            : this.props.pageNumber;
        if (newCurrentPageNumber !== this.props.pageNumber) {
            this.handleCurrentPageChange(newCurrentPageNumber);
        }
    };

    private handleNext = () => {
        const newCurrentPageNumber = this.props.pageNumber < this.props.pageCount
            ? this.props.pageNumber + 1
            : this.props.pageNumber;
        if (newCurrentPageNumber !== this.props.pageNumber) {
            this.handleCurrentPageChange(newCurrentPageNumber);
        }
    };

    private prepareNewCurrentBasedOnButtonClicked = (buttonValue) => {
        return Number.parseInt(buttonValue, 10);
    };

    private handleCurrentPageChange = (current) => {
        this.props.pageNumberSubject.next(current);
        window.scrollTo(0, 0);
    };

    private preparePaginationButtons = () => {
        const rows = [this.getSingleRow(this.props.pageNumber, true)];
        const indexBoundary = (index) => {
            return index <= this.props.pageNumber + PaginationComponent.PAGES_VISIBLE && index
                <= this.props.pageCount;
        };
        for (let index = this.props.pageNumber + 1; indexBoundary(index); index++) {
            rows.push(this.getSingleRow(index));
        }
        return rows;
    };

    private prepareLastButtons = () => {
        return this.props.pageNumber <= this.props.pageCount - PaginationComponent.PAGES_VISIBLE
            ? [this.getEllipsis(), this.getSingleRow(Math.floor(this.props.pageCount),)] : [];
    };

    private getSingleRow = (value, isCurrent = false) => {
        const className = isCurrent ? 'active'
            : ' ';
        return (<Pagination.Item
            className={className} onClick={this.handleClick} value={value}>{value}
        </Pagination.Item>);
    };

    private getEllipsis = () => {
        return (<Pagination.Ellipsis/>);
    };
}