import * as React from 'react';
import '../../Control.css';
import {Button, Col, Grid, Row} from 'react-bootstrap';
import {FormElement} from '../../../common/forms/FormElement';

const initialEvolutionProps = {
    evo_props: {
        fp_crossover_probability: '0.1',
        fp_individual_size: '8',
        fp_mutation_probability: '0.5',
        fp_population_size: '4',
        main_crossover_probability: '0.1',
        main_individual_size: '8',
        main_mutation_probability: '0.1',
        main_population_size: '4',
        stale_iterations_cnt: '1',
    },
    main_population: {
        batch_size: '128',
        convolution_size_multiplier: '2',
        dataset: 'MNIST',
        dense_size_multiplier: '1',
        learning_rate: '0.1',
        momentum: '0.9',
        optimizer: 'adam',
        rng_seed: '1',
        train_iterations: '1',
    },
    fp_population: {
        batch_size: '128',
        convolution_size_multiplier: '2',
        dataset: 'MNIST',
        dense_size_multiplier: '1',
        learning_rate: '0.1',
        momentum: '0.9',
        optimizer: 'adam',
        rng_seed: '0',
        train_iterations: '1'
    }
};

export class FormExperimentCreator extends React.Component<any, any> {
    public constructor(props) {
        super(props);
        this.state = {
            ...initialEvolutionProps,
            experimentName: '',
            maxIterations: undefined
        };
    }

    public render() {
        return (
            <Grid className="FormExperimentCreator-container">
                <Row style={{marginBottom: '30px'}}>
                    <Col md={2}>
                        <FormElement
                            controlId="name"
                            label="Experiment name"
                            value={this.state.experimentName}
                            onChange={this.onUpdateValue('experimentName')}
                        />
                    </Col>
                    <Col md={2}>
                        <FormElement
                            controlId="iter"
                            label="Max iterations"
                            value={this.state.maxIterations}
                            onChange={this.onUpdateValue('maxIterations')}
                        />
                    </Col>
                    <Col mdOffset={10} className="FormExperimentCreator-buttons-container">
                        <Button
                            bsSize="small"
                            bsStyle="primary"
                            className="JsonExperimentCreator-button"
                            onClick={this.createExperiment}
                        >
                            ACCEPT
                        </Button>
                        <Button
                            bsSize="small"
                            bsStyle="secondary"
                            className="JsonExperimentCreator-button"
                            onClick={this.props.onClose}
                        >
                            CANCEL
                        </Button>
                    </Col>
                </Row>
                <Row style={{overflowY: 'scroll', maxHeight: '40vh'}}>
                    <Col md={4} sm={12}>
                        <div className="FormExperimentCreator-section-title">
                            Evolution properties
                        </div>
                        {
                            Object.keys(initialEvolutionProps.evo_props).map((key) => (
                                this.renderFormElement('evo_props', key)
                            ))
                        }
                    </Col>
                    <Col md={4} sm={12}>
                        <div className="FormExperimentCreator-section-title">
                            Main population - DNN training properties
                        </div>
                        {
                            Object.keys(initialEvolutionProps.main_population).map((key) => (
                                this.renderFormElement('main_population', key)
                            ))
                        }
                    </Col>
                    <Col md={4} sm={12}>
                        <div className="FormExperimentCreator-section-title">
                            FP Population - DNN training properties
                        </div>
                        {
                            Object.keys(initialEvolutionProps.fp_population).map((key) => (
                                this.renderFormElement('fp_population', key)
                            ))
                        }
                    </Col>
                </Row>
            </Grid>
        );
    }

    private createExperiment = () => {
        this.props.createExperiment(
            JSON.stringify({
                ...this.state.evo_props,
                fp_train_config: this.state.fp_population,
                main_train_config: this.state.main_population
            }),
            this.state.experimentName,
            this.state.maxIterations
        );
        this.props.onClose();
    }

    private onUpdateValue = (propName) => (event) => {
        event.persist();
        this.setState({
            [propName]: event.target.value
        });
    }

    private updateProp = (propGroup, propName) => (event) => {
        event.persist();
        this.setState((prevState) => ({
            [propGroup]: {
                ...prevState[propGroup],
                [propName]: event.target.value
            }
        }));
    }

    private renderFormElement = (propGroup, propName) => {
        return (
            <FormElement
                controlId={`${propGroup}:${propName}`}
                label={propName}
                value={this.state[propGroup][propName]}
                onChange={this.updateProp(propGroup, propName)}
            />
        );
    }
}
