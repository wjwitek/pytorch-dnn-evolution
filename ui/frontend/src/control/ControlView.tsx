import * as React from "react";
import ExperimentMenuList from "./experiment-list";
import NewExperimentContainer from "./new-experiment";
import ExperimentStatusContainer from "./status";

interface ControlViewProps {
    notUsed?: string;
}

export const ControlView: React.StatelessComponent<ControlViewProps> = props => {
    return (
        <React.Fragment>
            <ExperimentMenuList/>
            <ExperimentStatusContainer/>
            <NewExperimentContainer/>
        </React.Fragment>
    );
};
