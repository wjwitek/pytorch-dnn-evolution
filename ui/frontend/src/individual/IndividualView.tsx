import * as React from 'react';
import {Scatter} from "react-chartjs-2";

interface Props {
    individual: any
}

export const IndividualView: React.StatelessComponent<Props> = (props) => {
    const generateColor = () => {
        return '#' + Math.random().toString(16).substr(-6);
    };
    const prepareDatasets = (points) => {
        return points.map(point => {
            return {
                label: point.name,
                data: point.x.map((val, idx) => {
                    return {x: val, y: point.y[idx]};
                }),
                backgroundColor: generateColor()
            };
        });
    };
    return (
        <div>
            <h4>Individual properties</h4>
            <table className="table table-hover table-bordered searchable">
                <tbody>
                <tr>
                    <td><b>Global id</b></td>
                    <td>{props.individual.id}</td>
                </tr>
                <tr>
                    <td><b>Type</b></td>
                    <td>{props.individual.type}</td>
                </tr>
                <tr>
                    <td><b>Fitness</b></td>
                    <td>{props.individual.fitness}</td>
                </tr>
                <tr>
                    <td><b>Parents</b></td>
                    <td>{props.individual.parents}</td>
                </tr>
                <tr>
                    <td><b>Genealogy Index</b></td>
                    <td>{props.individual.genealogy_index}</td>
                </tr>
                <tr>
                    <td><b>Iteration.Id</b></td>
                    <td>{props.individual.iteration_id}</td>
                </tr>
                <tr>
                    <td><b>Evaluation Time</b></td>
                    <td>{props.individual.evaluation_time}</td>
                </tr>

                </tbody>
            </table>
            <h4>Genotype</h4>
            <table className="table table-hover table-bordered searchable">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Position</th>
                    <th>Type</th>
                    <th>Extra</th>
                </tr>
                </thead>
                <tbody>
                {props.individual.genotype_formatted.map(gene => {
                    return (<tr key={gene.id}>
                        <td><span className="gene-id">{gene.id}</span></td>
                        <td><span className="gene-position">({gene.x}, {gene.y})</span></td>
                        <td><span className="gene-{gene_type}">
                        {gene.gene_type_value} ({gene.gene_type})
                    </span></td>
                        <td><span className="gene-extra">{gene.extra}</span></td>
                    </tr>);
                })
                }
                </tbody>
            </table>
            <h4>Individual structure</h4>
            <table className="table table-hover table-bordered searchable">
                <thead>
                <tr>
                    <th>Layer Id</th>
                    <th>Inputs</th>
                    <th>Outputs</th>
                    <th>Extras</th>
                </tr>
                </thead>
                <tbody>
                {props.individual.layers
                    .map(layer => {
                        return (<tr key={Math.random()}>
                            <td>{layer.id}</td>
                            <td>{JSON.stringify(layer.inputs)}</td>
                            <td>{JSON.stringify(layer.outputs)}</td>
                            <td>{JSON.stringify(layer.extras)}</td>
                        </tr>);
                    })
                }
                </tbody>
            </table>
            <h4>Model visualization</h4>
            <div>
                <Scatter data={{datasets: prepareDatasets(props.individual.points)}} options={{
                    scales: {
                        xAxes: [{
                            type: 'linear',
                            position: 'bottom'
                        }]
                    }
                }}/>
            </div>
        </div>
    );
};
