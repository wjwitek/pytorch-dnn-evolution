import * as React from 'react';
import {Component} from "react";
import {socketUrl} from "../api";

interface Props {
    importDataBase: (value: string) => void,
    response: string
}

export class DatabaseView extends Component<Props, any> {
    public constructor(props) {
        super(props);
        this.state = {
            textAreaValue: ""
        };
    }

    public render() {
        return (
            <div className="container">
                <div className="row">
                    <h3>Database dump</h3>
                    <form method="get" action={`${socketUrl}/dbdump.sql`}>
                        <button type="submit" className="btn btn-default">Download</button>
                    </form>
                </div>

                <div className="row">
                    <h3>Database load</h3>

                    <button type="submit" onClick={() => this.props.importDataBase(this.state.textAreaValue)}
                            className="btn btn-default">
                        Submit
                    </button>
                    <div className="form-group">
                        <textarea className="form-control" value={this.state.textAreaValue}
                                  onChange={this.handleChange}/>
                    </div>
                    <h4>Result</h4>
                    <code id="db-load-result">
                        {!!this.props.response ? this.props.response : 'empty'}
                    </code>
                </div>
            </div>
        );
    }

    public handleChange = (event) => {
        this.setState({textAreaValue: event.target.value});
    }
}