import * as React from 'react';
import { FormGroup, ControlLabel, FormControl, } from 'react-bootstrap';

interface Props {
    controlId: string,
    label: string,
    value: any,
    placeholder?: string,
    onChange: (event: any) => void
}

export const FormElement: React.StatelessComponent<Props> = (props) => {
    return (
        <FormGroup
            controlId={props.controlId}
        >
            <ControlLabel>
                {props.label}
            </ControlLabel>
            <FormControl
                type="text"
                value={props.value}
                placeholder={props.placeholder}
                onChange={props.onChange}
            />
        </FormGroup>
    );
};
