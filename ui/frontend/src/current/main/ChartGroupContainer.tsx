import * as React from "react";
import {
  SocketContextProps,
  withSocketContext
} from "../../common/SocketContext";
import { MainChartContainer } from "./MainChartContainer";

class ChartGroupContainer extends React.Component<SocketContextProps, any> {
  private mainTitle: string = "Main population - fitness progress over time";
  private fitnessTitle: string =
    "Fitness Predictors population - fitness progress over time";

  public constructor(props) {
    super(props);
    this.state = {
      fitness: {
        fp: { labels: [], values: [] },
        main: { labels: [], values: [] }
      },
      append: true
    };
    const socket = this.props.socket;
    socket.on("exp_fitness_update", this.handleFitnessUpdate);
    socket.on("exp_fitness_init", this.handleFitnessInit);
    socket.emit("exp_fitness_init_request");
  }

  public componentWillUnmount() {
    const socket = this.props.socket;
    socket.off("exp_fitness_update", this.handleFitnessUpdate);
    socket.off("exp_fitness_init", this.handleFitnessInit);
  }

  public render() {
    return (
      <div>
        <MainChartContainer
          labels={this.state.fitness.main.labels}
          values={this.state.fitness.main.values}
          title={this.mainTitle}
          append={this.state.append}
        />
        <MainChartContainer
          labels={this.state.fitness.fp.labels}
          values={this.state.fitness.fp.values}
          title={this.fitnessTitle}
          append={this.state.append}
        />
      </div>
    );
  }

  private handleFitnessUpdate = (message: any) => {
    console.log("handleFitnesUpdate");
    console.log(message);
    this.setState({
      fitness: message,
      append: true
    });
  };

  private handleFitnessInit = (message: any) => {
    console.log("handleFitnessInit");
    console.log(message);
    this.setState({
      fitness: message,
      append: false
    });
  };
}

export default withSocketContext(ChartGroupContainer);
