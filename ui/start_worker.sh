#!/usr/bin/env bash

if [ -e "/exthostname" ]; then
  HOSTNAME=`cat /exthostname`
else 
  HOSTNAME=`cat /etc/hostname`
fi

echo "Running celery with hostname: ${HOSTNAME}"

celery \
  -A torchxui.celery worker \
  --loglevel=DEBUG -n dnnevoworker@${HOSTNAME} ${EXTRA_OPTS}
