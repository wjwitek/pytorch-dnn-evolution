include tools/Makefile

TOOLS_DIR := ./tools
YAMLS_DIR := ./tools/kubernetes

#----------------------------------
# DNNEvo
LIB_DIR := ./lib
APP_DIR := ./ui

build-lib:
	@${INFO} Build 'torchx' library
	@make -C ${LIB_DIR} build

build-ui:
	@${INFO} Build 'torchxui' app
	@make -C ${APP_DIR} build

build: build-lib build-ui

install-lib: build-lib
	@${INFO} Install 'torchx' library
	@make -C ${LIB_DIR} install

install-ui: build-ui
	@${INFO} Install 'torchxui' app
	@make -C ${APP_DIR} install

install: install-lib install-ui

test-lib: install-lib
	@${INFO} Test 'torchx' library
	@make -C ${LIB_DIR} all_tests

test-ui: install
	@${INFO} Test 'torchxui' ui
	@make -C ${APP_DIR} all_tests

test-integration: docker-main
	@make kind-main
	@${INFO} Running integration tests
	@python3 ui/tests/test_integration.py
	@make kind-stop

test: test-lib test-ui

#----------------------------------
# DNNEvo image

VERSION := $(shell git rev-parse --short HEAD)
REGISTRY := pkoperek
IMAGE := pytorch-dnnevo
DEV_IMAGE := ${IMAGE}-dev
BUILD_ARGS := -q

docker-jupyter:
	docker build -f tools/dev.Dockerfile ${BUILD_ARGS} -t ${DEV_IMAGE}:latest .

run-jupyter: docker-jupyter
	docker run -it --rm -v ${PWD}:/srv -p 8081:8080 ${DEV_IMAGE}:latest

docker-main:
	@${INFO} Build ${REGISTRY}/${IMAGE}:${VERSION} image
	@docker build . -f Dockerfile --build-arg version=${VERSION} ${BUILD_ARGS} \
 		-t ${REGISTRY}/${IMAGE}:latest \
 		-t ${REGISTRY}/${IMAGE}:${VERSION}

docker-main-push: docker-main
	@${INFO} Push ${REGISTRY}/${IMAGE}:${VERSION}
	@docker push --all-tags ${REGISTRY}/${IMAGE}:${VERSION}

run-main: docker-main
	@${INFO} Starting containers
	@sed 's|pkoperek/pytorch-dnnevo:latest|${REGISTRY}/${IMAGE}:${VERSION}|' docker-compose.yml \
		| docker-compose -f - up

version:
	@echo Version: ${VERSION}
